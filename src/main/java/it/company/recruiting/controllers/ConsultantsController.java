package it.company.recruiting.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class ConsultantsController {

	@RequestMapping(value="/user/consultants", method= RequestMethod.GET)
	public String test(HttpServletResponse response) throws IOException {
		return "recruitingApp/index";
	}

}
