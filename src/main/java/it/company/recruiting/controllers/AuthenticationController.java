package it.company.recruiting.controllers;

import it.company.recruiting.models.AuthenticationCheck;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class AuthenticationController {

//	//TODO return userName With Roles
	@RequestMapping(value="checkAuthentication", method= RequestMethod.GET)
	@ResponseBody
	public AuthenticationCheck checkAuthentication(HttpServletResponse response) throws IOException {
		AuthenticationCheck authenticationCheck = new AuthenticationCheck();
		authenticationCheck.setAuthenticated(true);
		return authenticationCheck;
	}


	//TODO return userName With Roles
//	@RequestMapping(value="/checkAuthentication", method= RequestMethod.GET)
//	public void checkAuthentication(HttpServletResponse response) throws IOException {
//		AuthenticationCheck authenticationCheck = new AuthenticationCheck();
//		authenticationCheck.setAuthenticated(true);
//	}


}
