package it.company.recruiting.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;


@Controller
public class LoginController {

	@RequestMapping(value="/", method=RequestMethod.GET)
	public String entryPoint() throws IOException{
		return "redirect:/login";
	}

	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login() throws IOException{
		return "loginApp/index";
	}

}
