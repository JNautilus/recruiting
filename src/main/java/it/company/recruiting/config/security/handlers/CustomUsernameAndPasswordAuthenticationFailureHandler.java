package it.company.recruiting.config.security.handlers;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomUsernameAndPasswordAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
	                                    HttpServletResponse httpServletResponse,
	                                    AuthenticationException exception) throws IOException, ServletException {
			httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			/* La parte che segue dovrebbe essere rimossa. Infatti essendo servizi rest è demandata al client
			* la gestione di cosa succede se la richiesta non è autorizzata. Bisognerebbe
			* quindi rimuovere la parte di redirezionamento della request alla pagina di login. */
//			httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/login");
	}

}
