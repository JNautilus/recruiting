package it.company.recruiting.config.security.filters;


import com.nimbusds.jose.JOSEException;
import it.company.recruiting.config.security.services.JwtAuthenticationService;
import it.company.recruiting.keys.ConfigurationKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;


@Component
public class CustomJWTAuthenticationFilter extends GenericFilterBean {

	@Autowired
	private JwtAuthenticationService jwtAuthenticationService;

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,	ServletException {
			try {
					HttpServletRequest httpServletRequest = (HttpServletRequest) req;
							if((httpServletRequest.getHeader(ConfigurationKeys.X_AUTH_TOKEN.getValue())!=null)) {
									SecurityContextHolder.getContext().setAuthentication(jwtAuthenticationService.getAuthentication(httpServletRequest));
							}
			} catch (ParseException e) {
					e.printStackTrace();
			} catch (JOSEException e) {
					e.printStackTrace();
			}
			chain.doFilter(req, res); // always continue
	}


}