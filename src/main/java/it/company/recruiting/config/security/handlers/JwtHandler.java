package it.company.recruiting.config.security.handlers;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import it.company.recruiting.domains.User;
import it.company.recruiting.services.UserService;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class JwtHandler {


	@Autowired
	private UserService userService;

	private static final String API_SECRET = "S51-d7m8_lJ4MNRDaGzeMF-3u6fx8DfG7FfN-oMZyQc";
	private static final String ISS = "http://Recruiting";
	private static final long IAT = 33333222;

	public static String createToken(User user) throws JOSEException, ParseException  {
		// Create HMAC signer
			JWSSigner signer = new MACSigner(API_SECRET);
			// Prepare JWT with claims set
			JWTClaimsSet claimsSet = new JWTClaimsSet();
			claimsSet.setSubject(String.valueOf(user.getId()));
			claimsSet.setIssuer(ISS);
			claimsSet.setExpirationTime(new Date(new Date().getTime() + 60 * 1000));
//			Map<String,Object> customClaimsMap = new HashMap<String,Object>();
//			customClaimsMap.put("user",user);
			Set<String> roles = new HashSet();
			roles.add("admin");
			roles.add("user");
			claimsSet.setCustomClaim("roles", roles);
			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

			// Apply the HMAC protection
			signedJWT.sign(signer);
			// Serialize to compact form, produces something like
			// eyJhbGciOiJIUzI1NiJ9.SGVsbG8sIHdvcmxkIQ.onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA
			String serializedJwt = signedJWT.serialize();

			/* */
			// Cancellami subito
			JWSObject jwsObject = JWSObject.parse(serializedJwt);
			JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
			JWTClaimsSet jwtClaimsSet = JWTClaimsSet.parse(jsonPayload);
			/* */
			return serializedJwt;
	}



//	public User parseUserFromToken(String token) throws JOSEException, ParseException {
//			JWSObject jwsObject = JWSObject.parse(token);
//			JWSVerifier verifier = new MACVerifier(API_SECRET);
//			if (!jwsObject.verify(verifier)) {
//					throw new IllegalArgumentException("Fraudulent JWT token: " + token);
//			}
//			JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
//			JWTClaimsSet jwtClaimsSet = JWTClaimsSet.parse(jsonPayload);
//			String username = jwtClaimsSet.getSubject();
//
//			return null;
//
////		final String[] parts = token.split(SEPARATOR_SPLITTER);
////		if (parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0) {
////			try {
////				final byte[] userBytes = fromBase64(parts[0]);
////				final byte[] hash = fromBase64(parts[1]);
////
////				boolean validHash = Arrays.equals(createHmac(userBytes), hash);
////				if (validHash) {
////					final User user = fromJSON(userBytes);
////					if (new Date().getTime() < user.getExpires()) {
////						return user;
////					}
////				}
////			} catch (IllegalArgumentException e) {
////				//log tampering attempt here
////			}
////		}
////		return null;
////	}
//
//	}


	public User parseUserFromToken(String token) throws JOSEException, ParseException {
			JWSObject jwsObject = JWSObject.parse(token);
			JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
			JWTClaimsSet jwtClaimsSet = JWTClaimsSet.parse(jsonPayload);
			return userService.loadById(Long.valueOf(jwtClaimsSet.getSubject()));
	}

	public boolean isValidToken(String token) throws JOSEException, ParseException {
			JWSObject jwsObject = JWSObject.parse(token);
			JWSVerifier verifier = new MACVerifier(API_SECRET);
			return jwsObject.verify(verifier);
	}

	private boolean isValidToken(JWSObject jwsObject) throws JOSEException, ParseException {
			JWSVerifier verifier = new MACVerifier(API_SECRET);
			return jwsObject.verify(verifier);
	}


}
