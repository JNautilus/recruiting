package it.company.recruiting.config.security.services;

import com.nimbusds.jose.JOSEException;
import it.company.recruiting.config.security.handlers.JwtHandler;
import it.company.recruiting.config.security.models.AuthorizedUser;
import it.company.recruiting.domains.User;
import it.company.recruiting.keys.ConfigurationKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;


@Service
public class JwtAuthenticationService {


	@Autowired
	private JwtHandler jwtHandler;

	public void addAuthentication(HttpServletResponse httpServletResponse, AuthorizedUser authorizedUser) throws JOSEException, ParseException {
			final User user = authorizedUser;
			httpServletResponse.addHeader(ConfigurationKeys.X_AUTH_TOKEN.getValue(), JwtHandler.createToken(user));
	}

	public void addAuthenticationToCookies(HttpServletResponse httpServletResponse, AuthorizedUser authorizedUser) throws JOSEException, ParseException {
		final User user = authorizedUser;
		Cookie cookie = new Cookie(ConfigurationKeys.X_AUTH_TOKEN.getValue(), JwtHandler.createToken(user));
		httpServletResponse.addCookie(cookie);
	}

	public Authentication getAuthentication(HttpServletRequest request) throws ParseException, JOSEException {
			final String token = request.getHeader(ConfigurationKeys.X_AUTH_TOKEN.getValue());
			if (token != null) {
					final User user = jwtHandler.parseUserFromToken(token);
					if (user != null) {
							Authentication authentication;
							authentication = new UsernamePasswordAuthenticationToken(
																										user.getUsername(),
																										user.getPassword(),
																										new AuthorizedUser(user).getAuthorities()
																								);
							return authentication;
					}
			}
			return null;
	}



}
