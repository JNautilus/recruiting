package it.company.recruiting.config.security.models;

/**
 * Created by robertocrupi on 09/08/15.
 */

public class JsonResultWeb {

	private int code;
	private String message;
	private int status;
	//TO-DO
	//here declare token string

	private static final int SUCCESS = 0;
	private static final int ERROR = -1;
	private static final int WARNING = 1;


	public boolean isSuccess() {
		return code == 0 && status == SUCCESS;
	}

	public boolean isError() {
		return code == -1 && status == ERROR;
	}

	public boolean isWarning() {
		return code == 1 && status == WARNING;
	}

	public void setSuccess() {
		this.code = 0;
		this.status = SUCCESS;
	}

	public void setSuccess(String message) {
		this.setSuccess();
		this.message = message;
	}

	public void setError() {
		this.code = -1;
		this.status = ERROR;
	}

	public void setError(String message) {
		this.setError();
		this.message = message;
	}

	public void setWarning() {
		this.status = WARNING;
	}

	public void setWarning(String message) {
		this.setWarning();
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
