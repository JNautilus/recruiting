package it.company.recruiting.config.security.exceptions;


public class TokenExpiredException extends RuntimeException {
	public TokenExpiredException(String message) {
		super(message);
	}
}