package it.company.recruiting.config.security.services;

import it.company.recruiting.domains.User;
import it.company.recruiting.config.security.models.AuthorizedUser;
import it.company.recruiting.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component
public class CustomUserDetailsService implements UserDetailsService
{

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = userService.loadByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("UserName "+username+" not found");
        }
        return new AuthorizedUser(user);
    }


}