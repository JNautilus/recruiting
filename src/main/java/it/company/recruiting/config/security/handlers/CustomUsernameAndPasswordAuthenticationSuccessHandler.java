package it.company.recruiting.config.security.handlers;

import com.nimbusds.jose.JOSEException;
import it.company.recruiting.config.security.models.AuthorizedUser;
import it.company.recruiting.config.security.services.JwtAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;

@Component
public class CustomUsernameAndPasswordAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired
	private JwtAuthenticationService jwtAuthenticationService;


	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
	                                    HttpServletResponse httpServletResponse,
                                      Authentication authentication
																			) throws IOException, ServletException {

			AuthorizedUser authorizedUser = (AuthorizedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			try {
//				jwtAuthenticationService.addAuthentication(httpServletResponse,authorizedUser);
				jwtAuthenticationService.addAuthenticationToCookies(httpServletResponse,authorizedUser);
			} catch (JOSEException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}

			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			//httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/home");
	}

}