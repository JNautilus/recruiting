package it.company.recruiting.config.security.filters;

import it.company.recruiting.domains.User;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;

public class CustomUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private String jsonUsername;
	private String jsonPassword;

	@Override
	protected String obtainPassword(HttpServletRequest request) {
		String password;
		if (request.getHeader("Content-Type").matches("(.*)application/json(.*)")) {
			password = this.jsonPassword;
		}else{
			password = super.obtainPassword(request);
		}
		return password;
	}

	@Override
	protected String obtainUsername(HttpServletRequest request){
		String username;
		if (request.getHeader("Content-Type").matches("(.*)application/json(.*)")) {
			username = this.jsonUsername;
		}else{
			username = super.obtainUsername(request);
		}
		return username;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response){
			try {
				StringBuffer sb = new StringBuffer();
				String line = null;
				BufferedReader reader = request.getReader();
				while ((line = reader.readLine()) != null){
					sb.append(line);
				}
				ObjectMapper mapper = new ObjectMapper();
				User user = mapper.readValue(sb.toString(), User.class);
				this.jsonUsername = user.getUsername();
				this.jsonPassword = user.getPassword();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return super.attemptAuthentication(request, response);
	}

}
