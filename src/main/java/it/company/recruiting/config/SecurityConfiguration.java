package it.company.recruiting.config;

import it.company.recruiting.config.security.filters.CustomJWTAuthenticationFilter;
import it.company.recruiting.config.security.filters.CustomUsernameAndPasswordAuthenticationFilter;
import it.company.recruiting.config.security.handlers.CustomUsernameAndPasswordAuthenticationFailureHandler;
import it.company.recruiting.config.security.handlers.CustomUsernameAndPasswordAuthenticationSuccessHandler;
import it.company.recruiting.config.security.services.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Autowired
	private CustomUsernameAndPasswordAuthenticationSuccessHandler usernameAndPasswordAuthenticationSuccessHandler;

	@Autowired
	private CustomUsernameAndPasswordAuthenticationFailureHandler usernameAndPasswordAuthenticationFailureHandler;

	@Autowired
	private UsernamePasswordAuthenticationFilter authenticationFilter;

	@Autowired
	private CustomJWTAuthenticationFilter jWTAuthenticationFilter;

	@Bean
	@Autowired
	public UsernamePasswordAuthenticationFilter usernameAndPasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
			CustomUsernameAndPasswordAuthenticationFilter usernameAndPasswordAuthenticationFilter = new CustomUsernameAndPasswordAuthenticationFilter();
			usernameAndPasswordAuthenticationFilter.setUsernameParameter("username");
			usernameAndPasswordAuthenticationFilter.setPasswordParameter("password");
			usernameAndPasswordAuthenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/authenticate", "POST"));
			usernameAndPasswordAuthenticationFilter.setAuthenticationSuccessHandler(usernameAndPasswordAuthenticationSuccessHandler);
			usernameAndPasswordAuthenticationFilter.setAuthenticationFailureHandler(usernameAndPasswordAuthenticationFailureHandler);
			usernameAndPasswordAuthenticationFilter.setAuthenticationManager(authenticationManager);
			return usernameAndPasswordAuthenticationFilter;
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
	}

	@Override
  protected void configure(AuthenticationManagerBuilder registry) throws Exception {
      registry.userDetailsService(customUserDetailsService);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
      web.ignoring()
	        .antMatchers("/resources/**")
          .antMatchers("/webapp/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
	    http
			    .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class)
			    .addFilterBefore(jWTAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
			    .csrf().disable()
			    .authorizeRequests()
//			        .antMatchers("/web/**").permitAll()
				      .antMatchers("/admin/**").hasRole("ADMIN")
				      .antMatchers("/user/**").hasRole("USER")
		          .antMatchers("/web/**").permitAll()
				      .anyRequest().authenticated()
				      .and()
			    .formLogin()
			        .loginPage("/login")
			    .loginProcessingUrl("/authenticate")
					    .usernameParameter("username")
					    .passwordParameter("password")
					    .permitAll()
					    .and()
			    .logout()
			        .permitAll();
  }

}
