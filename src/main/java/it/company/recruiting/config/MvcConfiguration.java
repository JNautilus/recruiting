package it.company.recruiting.config;

import it.company.recruiting.keys.ConfigurationKeys;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@ComponentScan(basePackages="it.company.recruiting")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter{


	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/web/apps/");
		viewResolver.setSuffix(".html");
		return viewResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/web/apps/**").addResourceLocations("/web/apps/");
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				//.allowedOrigins("http://localhost:8080")
				.allowedOrigins("*")
				.allowedMethods("PUT", "DELETE")
				.allowedHeaders(ConfigurationKeys.X_AUTH_TOKEN.getValue())
				.exposedHeaders(ConfigurationKeys.X_AUTH_TOKEN.getValue())
				.allowCredentials(false).maxAge(3600);
	}

	
}
