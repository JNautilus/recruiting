package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by robertocrupi on 15/03/15.
 */
@Entity
@Table(name="LIVELLO_COMPETENZA")
public class LivelloCompetenza implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="LIVELLO_COMPETENZA")
    private String livelloCompetenza;

    @Column(name="ATTIVA")
    private Boolean attiva;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLivelloCompetenza() {
        return livelloCompetenza;
    }

    public void setLivelloCompetenza(String livelloCompetenza) {
        this.livelloCompetenza = livelloCompetenza;
    }

    public Boolean getAttiva() {
        return attiva;
    }

    public void setAttiva(Boolean attiva) {
        this.attiva = attiva;
    }

}
