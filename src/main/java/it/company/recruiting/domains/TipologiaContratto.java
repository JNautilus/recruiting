package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by robertocrupi on 11/03/15.
 */
@Entity
@Table(name="TIPOLOGIA_CONTRATTO")
public class TipologiaContratto implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="ATTIVA")
    private Boolean attiva;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Boolean getAttiva() {
        return attiva;
    }

    public void setAttiva(Boolean attiva) {
        this.attiva = attiva;
    }
}
