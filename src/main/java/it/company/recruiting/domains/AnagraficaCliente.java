package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by robertocrupi on 14/03/15.
 */

@Entity
@Table(name="ANAGRAFICA_CLIENTE")
public class AnagraficaCliente implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="RAGIONE_SOCIALE")
    private String ragioneSociale;

    @Column(name="NAZIONE")
    private String nazione;

    @Column(name="REGIONE")
    private String regione;

    @Column(name="PROVINCIA")
    private String provincia;

    @Column(name="COMUNE")
    private String comune;

    @Column(name="INDIRIZZO")
    private String indirizzo;

    @Column(name="TELEFONO")
    private String telefono;

    @Column(name="FAX")
    private String fax;

    @Column(name="EMAIL")
    private String email;

    @Column(name="SETTORE")
    private String settore;

    @Column(name="ATTIVA")
    private Boolean attiva;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getRegione() {
        return regione;
    }

    public void setRegione(String regione) {
        this.regione = regione;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getComune() {
        return comune;
    }

    public void setComune(String comune) {
        this.comune = comune;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSettore() {
        return settore;
    }

    public void setSettore(String settore) {
        this.settore = settore;
    }

    public Boolean getAttiva() {
        return attiva;
    }

    public void setAttiva(Boolean attiva) {
        this.attiva = attiva;
    }

}
