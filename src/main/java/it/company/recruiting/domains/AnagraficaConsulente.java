package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by robertocrupi on 09/03/15.
 */


@Entity
@Table(name="ANAGRAFICA_CONSULENTE")
public class AnagraficaConsulente implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="NOME")
    private String nome;

    @Column(name="COGNOME")
    private String cognome;

    @Column(name="CODICE_FISCALE")
    private String codiceFiscale;

    @Column(name="DATA_NASCITA")
    @Temporal(TemporalType.DATE)
    private Date dataNascita;

    @Column(name="INDIRIZZO_DOMICILIO")
    private String indirizzoDomicilio;

    @Column(name="REGIONE_DOMICILIO")
    private String regioneDomicilio;

    @Column(name="PROVINCIA_DOMICILIO")
    private String provinciaDomicilio;

    @Column(name="COMUNE_DOMICILIO")
    private String comuneDomicilio;

    @Column(name="CAP_DOMICILIO")
    private String capDomicilio;

    @Column(name="INDIRIZZO_RESIDENZA")
    private String indirizzoResidenza;

    @Column(name="REGIONE_RESIDENZA")
    private String regioneResidenza;

    @Column(name="PROVINCIA_RESIDENZA")
    private String provinciaResidenza;

    @Column(name="COMUNE_RESIDENZA")
    private String comuneResidenza;

    @Column(name="CAP_RESIDENZA")
    private String capResidenza;

    @Column(name="TELEFONO")
    private String telefono;

    @Column(name="CELLULARE")
    private String cellulare;

    @Column(name="EMAIL")
    private String email;

    @Column(name="ATTIVA")
    private Boolean attiva;

    //Bidirectional relationship
    @OneToMany(fetch = FetchType.LAZY, mappedBy="anagraficaConsulente")
    private List<Contratto> contratto;

    //unidirectional relationship
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_CONSULENTE",referencedColumnName = "ID")
    private List<CurriculumVitae> curriculumVitaes;

    //unidirectional relationship
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_CONSULENTE",referencedColumnName = "ID")
    private List<Commessa> commesse;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getIndirizzoDomicilio() {
        return indirizzoDomicilio;
    }

    public void setIndirizzoDomicilio(String indirizzoDomicilio) {
        this.indirizzoDomicilio = indirizzoDomicilio;
    }

    public String getRegioneDomicilio() {
        return regioneDomicilio;
    }

    public void setRegioneDomicilio(String regioneDomicilio) {
        this.regioneDomicilio = regioneDomicilio;
    }

    public String getProvinciaDomicilio() {
        return provinciaDomicilio;
    }

    public void setProvinciaDomicilio(String provinciaDomicilio) {
        this.provinciaDomicilio = provinciaDomicilio;
    }

    public String getComuneDomicilio() {
        return comuneDomicilio;
    }

    public void setComuneDomicilio(String comuneDomicilio) {
        this.comuneDomicilio = comuneDomicilio;
    }

    public String getCapDomicilio() {
        return capDomicilio;
    }

    public void setCapDomicilio(String capDomicilio) {
        this.capDomicilio = capDomicilio;
    }

    public String getIndirizzoResidenza() {
        return indirizzoResidenza;
    }

    public void setIndirizzoResidenza(String indirizzoResidenza) {
        this.indirizzoResidenza = indirizzoResidenza;
    }

    public String getRegioneResidenza() {
        return regioneResidenza;
    }

    public void setRegioneResidenza(String regioneResidenza) {
        this.regioneResidenza = regioneResidenza;
    }

    public String getProvinciaResidenza() {
        return provinciaResidenza;
    }

    public void setProvinciaResidenza(String provinciaResidenza) {
        this.provinciaResidenza = provinciaResidenza;
    }

    public String getComuneResidenza() {
        return comuneResidenza;
    }

    public void setComuneResidenza(String comuneResidenza) {
        this.comuneResidenza = comuneResidenza;
    }

    public String getCapResidenza() {
        return capResidenza;
    }

    public void setCapResidenza(String capResidenza) {
        this.capResidenza = capResidenza;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCellulare() {
        return cellulare;
    }

    public void setCellulare(String cellulare) {
        this.cellulare = cellulare;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAttiva() {
        return attiva;
    }

    public void setAttiva(Boolean attiva) {
        this.attiva = attiva;
    }

    public List<Contratto> getContratto() {
        return contratto;
    }

    public void setContratto(List<Contratto> contratto) {
        this.contratto = contratto;
    }

    public List<CurriculumVitae> getCurriculumVitaes() {
        return curriculumVitaes;
    }
    public void setCurriculumVitaes(List<CurriculumVitae> curriculumVitaes) {
        this.curriculumVitaes = curriculumVitaes;
    }

}
