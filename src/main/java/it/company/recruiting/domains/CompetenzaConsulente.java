package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by robertocrupi on 15/03/15.
 */
public class CompetenzaConsulente implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="ID_CONSULENTE")
    private long idConsulente;

    //Unidirectional
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_COMPETENZA")
    private Competenza competenza;

    //Unidirectional
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_LIVELLO_COMPETENZA")
    private LivelloCompetenza livellocompetenza;






}
