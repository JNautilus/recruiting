package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robertocrupi on 09/03/15.
 */

@Entity
@Table(name="CONTRATTO")
public class Contratto implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    //Bidirectional relationship
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ID_CONSULENTE")
    private AnagraficaConsulente anagraficaConsulente;

    //Unidirectional
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_TIPOLOGIA_CONTRATTO")
    private TipologiaContratto tipologiaContratto;

    @Column(name="DATA_INSERIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataInserimento;

    @Column(name="PERCORSO_DOC")
    private String percorsoDocumento;

    @Column(name="ATTIVO")
    private Boolean attivo;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AnagraficaConsulente getAnagraficaConsulente() {
        return anagraficaConsulente;
    }

    public void setAnagraficaConsulente(AnagraficaConsulente anagraficaConsulente) {
        this.anagraficaConsulente = anagraficaConsulente;
    }

    public TipologiaContratto getTipologiaContratto() {
        return tipologiaContratto;
    }

    public void setTipologiaContratto(TipologiaContratto tipologiaContratto) {
        this.tipologiaContratto = tipologiaContratto;
    }

    public Date getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public String getPercorsoDocumento() {
        return percorsoDocumento;
    }

    public void setPercorsoDocumento(String percorsoDocumento) {
        this.percorsoDocumento = percorsoDocumento;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }


}
