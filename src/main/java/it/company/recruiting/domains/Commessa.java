package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robertocrupi on 14/03/15.
 */
@Entity
@Table(name="COMMESSA")
public class Commessa implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="ID_CONSULENTE")
    private long idConsulente;

    @Column(name="ID_CLIENTE")
    private long idCliente;

    @Column(name="COSTO_GIORNATA_UOMO")
    private Float costoGiornataUomo;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="DATA_INIZIO")
    @Temporal(TemporalType.DATE)
    private Date dataInizio;

    @Column(name="DATA_FINE")
    @Temporal(TemporalType.DATE)
    private Date dataFine;

    @Column(name="NOTA")
    private String nota;

    @Column(name="ATTIVA")
    private Boolean attiva;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdConsulente() {
        return idConsulente;
    }

    public void setIdConsulente(long idConsulente) {
        this.idConsulente = idConsulente;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public Float getCostoGiornataUomo() {
        return costoGiornataUomo;
    }

    public void setCostoGiornataUomo(Float costoGiornataUomo) {
        this.costoGiornataUomo = costoGiornataUomo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Date getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Date dataInizio) {
        this.dataInizio = dataInizio;
    }

    public Date getDataFine() {
        return dataFine;
    }

    public void setDataFine(Date dataFine) {
        this.dataFine = dataFine;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Boolean getAttiva() {
        return attiva;
    }

    public void setAttiva(Boolean attiva) {
        this.attiva = attiva;
    }

}
