package it.company.recruiting.domains;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robertocrupi on 11/03/15.
 */

@Entity
@Table(name="CURRICULUM_VITAE")
public class CurriculumVitae  implements Serializable {

    private static final long serialVersionUID=1L;

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="NOME")
    private String nome;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="ID_COSULENTE")
    private long idConsulente;

    @Column(name="DATA_INSERIMENTO")
    @Temporal(TemporalType.DATE)
    private Date dataInserimento;

    @Column(name="PERCORSO_DOC")
    private String percorsoDoc;

    @Column(name="ATTIVO")
    private Boolean attivo;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public long getIdConsulente() {
        return idConsulente;
    }

    public void setIdConsulente(long idConsulente) {
        this.idConsulente = idConsulente;
    }

    public Date getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public String getPercorsoDoc() {
        return percorsoDoc;
    }

    public void setPercorsoDoc(String percorsoDoc) {
        this.percorsoDoc = percorsoDoc;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

}
