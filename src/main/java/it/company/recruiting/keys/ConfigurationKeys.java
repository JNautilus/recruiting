package it.company.recruiting.keys;


public enum ConfigurationKeys {

	X_AUTH_TOKEN("X-Auth-Token");

	private final String value;

	private ConfigurationKeys(String value){
		this.value=value;
	}

	public String getValue() {
		return value;
	}



}
