/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.company.recruiting.dao;


import java.io.Serializable;
import java.util.List;


/**
 *
 * @author rcrupi
 * @param <Entity>
 * @param <Id>
 */
public interface GenericDao<Entity, Id extends Serializable> {
      
    public Entity loadById(Id id);
    public long countAllRows();
    public int countAllRowsFromDB();
    public List<Entity> getEntity();
    public void save(final Entity entity);
    public void updateList(List<Entity> entityList);
    public void update(Entity entity);    
    public void delete(Entity entity); 

}
