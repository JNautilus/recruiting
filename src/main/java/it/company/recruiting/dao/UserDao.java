/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.company.recruiting.dao;

import it.company.recruiting.domains.User;
import java.util.List;


public interface UserDao extends GenericDao<User, Long> {

    List<User> loadByUsername(String username);
    List<User> loadByUsernameAndPassword(String username, String password);

}