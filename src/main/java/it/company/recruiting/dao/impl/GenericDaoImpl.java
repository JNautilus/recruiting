package it.company.recruiting.dao.impl;


import it.company.recruiting.dao.GenericDao;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


@Transactional
public abstract class GenericDaoImpl<Entity, Id extends Serializable> implements GenericDao<Entity, Id> {

    //private static final Logger logger = Logger.getLogger(GenericDaoImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    private Class<Entity> entityClass;

    public GenericDaoImpl() {
        this.entityClass = (Class<Entity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    protected void AbstractDaoImpl(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }
   
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
 
    @SuppressWarnings("unchecked")
    @Override
    public Entity loadById(Id id){
        return (Entity) sessionFactory.getCurrentSession().get(entityClass, id);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Entity> getEntity(){
        return sessionFactory.getCurrentSession().createQuery("from " + entityClass.getName()).list();    	
    }

    @Override
    public long countAllRows(){      
        return ((Long) getCurrentSession()
                       .createQuery("select count(*) from " + entityClass.getName())
                       .uniqueResult()
                );
    }

    @Override
    public int countAllRowsFromDB(){
        Query query = getCurrentSession().createQuery("select count(*) from " + entityClass.getName());
        Integer result = Integer.parseInt(String.valueOf(query.uniqueResult()));        
        if (result==null){
            return 0;
        } else{
            return result;
        }
    }    

    @SuppressWarnings("unchecked")
    @Override
    public void save(final Entity entity){
        sessionFactory.getCurrentSession().save(entity);
    }

    
    @Override
    public void updateList(List<Entity> entityList) {        
        for (Entity entity : entityList){
            getCurrentSession().update(entity);            
        }
    }
    
    @Override
    public void update(Entity entity) {
        getCurrentSession().update(entity);
    }
    
    
    @Override
    public void delete(Entity entity) {
        getCurrentSession().delete(entity);
    }

    
}