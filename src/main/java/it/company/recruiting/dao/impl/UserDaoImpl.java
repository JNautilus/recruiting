package it.company.recruiting.dao.impl;


import it.company.recruiting.dao.UserDao;
import it.company.recruiting.domains.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class UserDaoImpl extends GenericDaoImpl<User,Long> implements UserDao {


    @Override
    public List<User> loadByUsernameAndPassword(String username, String password){
        Criteria criteria = getCurrentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("username", username));
        criteria.add(Restrictions.eq("password", password));
        return criteria.list();
    }

    @Override
    public List<User> loadByUsername(String username){
        Criteria criteria = getCurrentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("username", username));
        return criteria.list();
    }


}
