package it.company.recruiting.services.impl;

import it.company.recruiting.dao.UserDao;
import it.company.recruiting.domains.User;
import it.company.recruiting.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User loadByUsername(String username){
        List<User> users
                = userDao.loadByUsername(username);
        return users.get(0);
    }

		@Override
		public User loadById(Long id) {
			return userDao.loadById(id);
		}


}
