package it.company.recruiting.services;

import it.company.recruiting.domains.User;


public interface UserService {

    User loadByUsername(String username);
		User loadById(Long id);

}
