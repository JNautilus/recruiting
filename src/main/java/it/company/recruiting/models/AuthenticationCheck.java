package it.company.recruiting.models;


public class AuthenticationCheck {

	private boolean isAuthenticated;

	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

}
