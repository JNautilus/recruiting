require.config({
    baseUrl: 'web/apps/recruitingApp/js/',
    paths: {
        'angular': 'lib/angular/angular',
        'angular-route': 'lib/angular-route/angular-route',
	      'angular-bootstrap': 'lib/angular-bootstrap/ui-bootstrap-tpls',
	      'angular-cookies': 'lib/angular-cookies/angular-cookies',
        'ngTable': 'lib/ng-table/dist/ng-table'
    },
    shim: {
            'angular': {
                exports: 'angular'
            },
				    'angular-bootstrap': {
					    exports: 'angular-bootstrap',
					    deps: ['angular']
				    },
            'angular-route': {
                exports: 'angular-route',
                deps: ['angular']
            },
						'angular-cookies': {
							exports: 'angular-cookies',
							deps: ['angular']
						},
            'ngTable': {
                exports: 'ngTable',
                deps: ['angular']
            },
				    'app': {
					    deps: ['angular', 'angular-bootstrap', 'angular-route','ngTable','angular-cookies']
				    }
	        }
		});

require(['app'],function(app){
    angular.bootstrap(document, ['app']);
});





