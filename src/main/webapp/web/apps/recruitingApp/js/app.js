define(['components/services/dependencyResolverFor','appConfigLoader'], function(dependencyResolverFor,appConfigLoader){

    var app = angular.module('app', ['ui.bootstrap','ngRoute','ngTable','ngCookies']);
    var routes = appConfigLoader.getRoutes();

    app.constant('CONFIGURATION', appConfigLoader.getGlobal());

    app.controller('ApplicationController', ['$scope', '$injector', function($scope, $injector) {
        require(['components/controllers/applicationController'], function(applicationController) {
            $injector.invoke(applicationController, this, {'$scope': $scope});
        });
    }]);


    //used to trigger all path changes
    app.run(function ($rootScope,$injector,$location,$http,$cookies) {
	    $rootScope.$on('$routeChangeStart', function (event, next,current) {
	        console.log("change root scope");
	        var cookie = $cookies.get("X-Auth-Token");
	        console.log("current root is = " + $location.path());
	        if(routes.ROUTES.hasOwnProperty($location.path())){
		        var url="checkAuthentication";
		        $http({
			        method: "GET",
			        url: url,
			        headers: {
				        "X-Auth-Token": cookie
			        }
		        }).success(function(data, status, headers, config) {
			        console.log("success");
		        }).error(function(data, status, headers, config) {
			        console.log("failure");
		        });
	        }
        });
    });

    app.config(
    [
        '$routeProvider',
        '$locationProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$provide',

        function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
	        app.controller = $controllerProvider.register;
	        app.directive  = $compileProvider.directive;
	        app.filter     = $filterProvider.register;
	        app.factory    = $provide.factory;
	        app.service    = $provide.service;

            if(routes.ROUTES !== undefined) {
                angular.forEach(routes.ROUTES, function(route, path) {
                    $routeProvider.when(path, {
                        templateUrl:route.templateUrl,
                        resolve:dependencyResolverFor(route.dependencies)
                    });
                });
            }

            if(routes.DEFAULT_ROUTE_PATH !== undefined) {
                $routeProvider.otherwise({redirectTo:routes.DEFAULT_ROUTE_PATH});
            }
        }
    ]);

   return app;
});