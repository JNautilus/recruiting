define('appConfigLoader', [
		"config/routes",
		"config/userRoles",
		"config/authEvents",
		"config/restInfo"
		], function(routes,userRoles,authEvents,restInfo) {
    return {
		getGlobal: function(){
			var constants = [routes,userRoles,authEvents,restInfo];
			var configuration = {};
			angular.forEach(constants, function(iValue,iKey) {
				var constant = iValue;
				angular.forEach(constant, function(jValue,jKey) {
			    	configuration[jKey] = jValue;
				})
			})
			return configuration;
		},
        getRoutes: function() {
            return routes;
        },    	
        getUserRoles: function() {
            return userRoles;
        },
        getAuthEvents: function() {
            return authEvents;
        },
        getRestInfo: function() {
            return restInfo;
        }        
    }
});