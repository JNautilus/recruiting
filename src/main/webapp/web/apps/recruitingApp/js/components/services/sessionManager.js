define(['components/models/session'], function(session) {

	var sessionManager = {};

	sessionManager.createSession= function(sessionObj){
		session.userId = sessionObj.userId;
		session.userRole = sessionObj.userRole;
		session.token = sessionObj.token;
		return session;
	};

	sessionManager.destroySession= function(){
		session.userId = '';
		session.userRole = '';
		session.token = '';
		return session;
	};

	sessionManager.getSession= function(){
		return session;
	};

    return sessionManager;

});