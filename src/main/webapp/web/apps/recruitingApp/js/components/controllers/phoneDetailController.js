define(['app'], function(app){
    app.controller('PhoneDetailController', ['$scope','$routeParams','$http',
        function($scope, $routeParams, $http) {
		    $http.get('phones/' + $routeParams.phoneId + '.json').success(function(data) {
		      	$scope.phone = data;
		    });
        }]);
}); 