define(['app'], function(app){
    app.filter('checkmark', function() {
  		return function(input) {
    		return input ? '\u2713' : '\u2718';
  		};
	});
});