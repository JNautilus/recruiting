define(['app'], function(app) {
    return ['$scope','$http','CONFIGURATION', function($scope, $http, CONFIGURATION) {

        console.log("ApplicationController");

        $scope.currentUser = null;
        $scope.currentSession = null;


        $scope.setCurrentUser = function (user) {
            $scope.currentUser = user;
        };
        $scope.setSession = function (session) {
            $scope.currentSession = session;
        };

        $scope.$apply();
    }];
});