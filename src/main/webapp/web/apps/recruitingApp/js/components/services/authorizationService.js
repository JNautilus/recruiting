define(['app','components/services/sessionManager'], function(app, sessionManager){

    app.service('AuthorizationService', function($http) {

        this.login = function (username, password) {
            var session={};
            session.userId = 1;
            session.userRole = 'Admin';
            session.token = 'token';
            sessionManager.createSession(session);
            var user={};

            if(username==='pippo' && password==='pappo'){
                console.log("AuthorizationService: valid credentials");
                user.username = 'pippo';
                user.password = 'pappo';
            }

            console.log("session from auth service; userId = " + sessionManager.getSession().userId);
            return user;
            /*
            return $http
                .post('/login', credentials)
                .then(function (res) {
                    sessionManager.createSession(res.data.id, res.data.user.id,
                        res.data.user.role);
                    return res.data.user;
                });
            */
        };
    });

});