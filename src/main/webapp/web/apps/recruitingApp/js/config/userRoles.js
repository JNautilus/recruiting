define(function(userRoles) {
	return {
        USER_ROLES: {
	  		all: '*',
			admin: 'admin',
			editor: 'editor',
			guest: 'guest'
        }
	}
});