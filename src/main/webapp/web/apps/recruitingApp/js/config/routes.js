define([], function() {
    return {
        DEFAULT_ROUTE_PATH: '/phones',
        ROUTES: {
		        '/dashboard': {
			        url:'/dashboard',
			        templateUrl: 'web/apps/recruitingApp/'+'views/partials/dashboard.html',
			        dependencies: [
				        'components/controllers/dashboardController'
			        ]
		        },
            '/login': {
	              url:'/login',
                templateUrl: 'web/apps/recruitingApp/'+'views/partials/login.html',
                dependencies: [
                    'components/controllers/loginController',
                    'components/services/authorizationService'
                ]
            },
            '/phones': {
	              url:'/phones',
                templateUrl: 'web/apps/recruitingApp/'+'views/partials/phone-list.html',
                dependencies: [
                    'components/controllers/phoneListController'
                ]
            },
            '/phones/:phoneId': {
	              url:'/phones/:phoneId',
                templateUrl: 'web/apps/recruitingApp/'+'views/partials/phone-detail.html',
                dependencies: [
                    'components/controllers/phoneDetailController',
                    'components/filters/phoneListDetailFilter'
                ]
            },
            '/users': {
	              url:'/users',
                templateUrl: 'web/apps/recruitingApp/'+'views/partials/users.html',
                dependencies: [
                    'components/controllers/usersController'
                ]
            }
        }
    };
});
