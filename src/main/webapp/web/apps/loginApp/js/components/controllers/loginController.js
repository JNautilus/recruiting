angular
		.module('loginApp.controllers.loginController', ['ngCookies'])
		.controller("LoginController",[ "$scope", "$http", "$location", "$window", "$cookies",function($scope, $http, $location, $window, $cookies) {
			console.log("LoginController");
			$scope.showLoginErrMsg=false;
			$scope.loginForm = function () {
				var user = $scope.user;
				console.log(user);
				var url = $location.absUrl().substr(0, $location.absUrl().lastIndexOf("/"));
				console.log("url = " +url);
				$http.post(url+'/authenticate',user).
						success(function(data, status, headers, config) {
							console.log("success token = " +headers()["x-auth-token"]);
							$window.location.href = url+'/home';
//							var xAuthToken=headers()["x-auth-token"];
//							$http({method: 'GET', url: url+'/home', headers: {'X-Auth-Token': xAuthToken}});
						}).
						error(function(data, status, headers, config) {
							$scope.showLoginErrMsg=true;
							console.log("error");
						});
			}
		}])