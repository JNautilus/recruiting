var module = angular.module("loginApp", [ 'ui.bootstrap', 'ngRoute', 'loginApp.controllers.loginController']);

module.config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/login', {
                templateUrl:'web/apps/loginApp/views/partials/login.html',
                controller:'LoginController'
            }).
            otherwise({
                templateUrl:'web/apps/loginApp/views/partials/login.html',
                controller:'LoginController'
            });
    }]);
